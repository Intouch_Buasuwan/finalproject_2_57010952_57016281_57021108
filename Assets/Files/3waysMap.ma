//Maya ASCII 2014 scene
//Name: 3waysMap.ma
//Last modified: Fri, Mar 25, 2016 04:51:52 PM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.1414291278835016 15.385898649134543 -38.217550878213963 ;
	setAttr ".r" -type "double3" -22.538352730407379 538.9999999999842 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 37.397098943215802;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "left";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -100.1 0 0 ;
	setAttr ".r" -type "double3" 0 -89.999999999999986 0 ;
createNode camera -n "leftShape" -p "left";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "left1";
	setAttr ".den" -type "string" "left1_depth";
	setAttr ".man" -type "string" "left1_mask";
	setAttr ".hc" -type "string" "viewSet -ls %camera";
	setAttr ".o" yes;
createNode transform -n "group";
	setAttr ".s" -type "double3" 3.9920397239695244 3.9920397239695244 3.9920397239695244 ;
	setAttr ".rp" -type "double3" -5.3644180297851563e-007 0.49251514230308846 0 ;
	setAttr ".sp" -type "double3" -5.3644180297851563e-007 0.49251514230308846 0 ;
createNode transform -n "group1";
	setAttr ".s" -type "double3" -1.0010563815386913 1 1 ;
createNode transform -n "pPlane1" -p "group1";
	setAttr ".t" -type "double3" 0 0.05038781798375358 0 ;
	setAttr ".s" -type "double3" 6 1 24 ;
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pPlane2" -p "group1";
	setAttr ".t" -type "double3" 0 0.05038781798375358 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999972 0 ;
	setAttr ".s" -type "double3" 6 1 24.712584419590854 ;
	setAttr ".rp" -type "double3" -5.3447605771713282e-015 0 -12.035330871866655 ;
	setAttr ".rpt" -type "double3" -12.03533087186665 0 12.035330871866655 ;
	setAttr ".sp" -type "double3" -8.9079342952855483e-016 0 -0.50147211966111072 ;
	setAttr ".spt" -type "double3" -4.4539671476427785e-015 0 -11.533858752205544 ;
createNode mesh -n "pPlaneShape2" -p "pPlane2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".pt";
	setAttr ".pt[6]" -type "float3" 0 0.00089843606 0 ;
	setAttr ".pt[7]" -type "float3" 0 0.005771236 0 ;
	setAttr ".pt[8]" -type "float3" 0 0.2409716 0 ;
	setAttr ".pt[9]" -type "float3" 0 0.043799672 0 ;
	setAttr ".pt[20]" -type "float3" -9.1163071e-018 0.078570887 0 ;
	setAttr ".pt[21]" -type "float3" -2.7755576e-017 0.15947834 0 ;
createNode mesh -n "polySurfaceShape1" -p "pPlane2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 55 ".uvst[0].uvsp[0:54]" -type "float2" 0 0.60000002 0.1
		 0.60000002 0.2 0.60000002 0.30000001 0.60000002 0.40000001 0.60000002 0.5 0.60000002
		 0.60000002 0.60000002 0.69999999 0.60000002 0.80000001 0.60000002 0.90000004 0.60000002
		 1 0.60000002 0 0.69999999 0.1 0.69999999 0.2 0.69999999 0.30000001 0.69999999 0.40000001
		 0.69999999 0.5 0.69999999 0.60000002 0.69999999 0.69999999 0.69999999 0.80000001
		 0.69999999 0.90000004 0.69999999 1 0.69999999 0 0.80000001 0.1 0.80000001 0.2 0.80000001
		 0.30000001 0.80000001 0.40000001 0.80000001 0.5 0.80000001 0.60000002 0.80000001
		 0.69999999 0.80000001 0.80000001 0.80000001 0.90000004 0.80000001 1 0.80000001 0
		 0.90000004 0.1 0.90000004 0.2 0.90000004 0.30000001 0.90000004 0.40000001 0.90000004
		 0.5 0.90000004 0.60000002 0.90000004 0.69999999 0.90000004 0.80000001 0.90000004
		 0.90000004 0.90000004 1 0.90000004 0 1 0.1 1 0.2 1 0.30000001 1 0.40000001 1 0.5
		 1 0.60000002 1 0.69999999 1 0.80000001 1 0.90000004 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 -3.7252903e-009 ;
	setAttr ".pt[1]" -type "float3" 0 0 -3.7252903e-009 ;
	setAttr ".pt[2]" -type "float3" 0 0 -3.7252903e-009 ;
	setAttr ".pt[3]" -type "float3" 0 0 -3.7252903e-009 ;
	setAttr -s 55 ".vt[0:54]"  -0.5 2.2204466e-017 -0.12455679 -0.48649997 2.2204466e-017 -0.12455679
		 -0.47015691 2.2204466e-017 -0.12455679 -0.45389089 2.2204466e-017 -0.12455679 -0.20153376 2.2204466e-017 -0.12455679
		 0 2.2204466e-017 -0.12455679 0.14133637 2.2204466e-017 -0.12455679 0.44321984 2.2204466e-017 -0.12455679
		 0.45991737 2.2204466e-017 -0.12455679 0.48127407 2.2204466e-017 -0.12455679 0.5 2.2204466e-017 -0.12455679
		 -0.5 4.4408918e-017 -0.19999999 -0.48649997 4.4408918e-017 -0.19999999 -0.47015691 4.4408918e-017 -0.19999999
		 -0.45389089 4.4408918e-017 -0.19999999 -0.20153376 4.4408918e-017 -0.19999999 0 4.4408918e-017 -0.19999999
		 0.14133637 4.4408918e-017 -0.19999999 0.44321984 4.4408918e-017 -0.19999999 0.45991737 4.4408918e-017 -0.19999999
		 0.48127407 4.4408918e-017 -0.19999999 0.5 4.4408918e-017 -0.19999999 -0.5 6.6613384e-017 -0.30000001
		 -0.48649997 6.6613384e-017 -0.30000001 -0.47015691 6.6613384e-017 -0.30000001 -0.45389089 6.6613384e-017 -0.30000001
		 -0.20153376 6.6613384e-017 -0.30000001 0 6.6613384e-017 -0.30000001 0.14133637 6.6613384e-017 -0.30000001
		 0.44321984 6.6613384e-017 -0.30000001 0.45991737 6.6613384e-017 -0.30000001 0.48127407 6.6613384e-017 -0.30000001
		 0.5 6.6613384e-017 -0.30000001 -0.5 8.881785e-017 -0.40000004 -0.48649997 8.881785e-017 -0.40000004
		 -0.47015691 8.881785e-017 -0.40000004 -0.45389089 8.881785e-017 -0.40000004 -0.20153376 8.881785e-017 -0.40000004
		 0 8.881785e-017 -0.40000004 0.14133637 8.881785e-017 -0.40000004 0.44321984 8.881785e-017 -0.40000004
		 0.45991737 8.881785e-017 -0.40000004 0.48127407 8.881785e-017 -0.40000004 0.5 8.881785e-017 -0.40000004
		 -0.5 1.110223e-016 -0.5 -0.48649997 1.110223e-016 -0.5 -0.47015691 1.110223e-016 -0.5
		 -0.45389089 1.110223e-016 -0.5 -0.20153376 1.110223e-016 -0.5 0 1.110223e-016 -0.5
		 0.14133637 1.110223e-016 -0.5 0.44321984 1.110223e-016 -0.5 0.45991737 1.110223e-016 -0.5
		 0.48127407 1.110223e-016 -0.5 0.5 1.110223e-016 -0.5;
	setAttr -s 90 ".ed[0:89]"  0 1 0 0 11 0 1 2 0 1 12 1 2 3 0 2 13 1 3 4 0
		 3 14 1 4 5 0 4 15 1 5 6 0 6 7 0 6 17 1 7 8 0 7 18 1 8 9 0 8 19 1 9 10 0 9 20 1 10 21 0
		 11 12 1 11 22 0 12 13 1 12 23 1 13 14 1 13 24 1 14 15 1 14 25 1 15 16 1 15 26 1 16 17 1
		 17 18 1 17 28 1 18 19 1 18 29 1 19 20 1 19 30 1 20 21 1 20 31 1 21 32 0 22 23 1 22 33 0
		 23 24 1 23 34 1 24 25 1 24 35 1 25 26 1 25 36 1 26 27 1 26 37 1 27 28 1 28 29 1 28 39 1
		 29 30 1 29 40 1 30 31 1 30 41 1 31 32 1 31 42 1 32 43 0 33 34 1 33 44 0 34 35 1 34 45 1
		 35 36 1 35 46 1 36 37 1 36 47 1 37 38 1 37 48 1 38 39 1 39 40 1 39 50 1 40 41 1 40 51 1
		 41 42 1 41 52 1 42 43 1 42 53 1 43 54 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0
		 50 51 0 51 52 0 52 53 0 53 54 0;
	setAttr -s 36 -ch 152 ".fc[0:35]" -type "polyFaces" 
		f 4 0 3 -21 -2
		mu 0 4 0 1 12 11
		f 4 2 5 -23 -4
		mu 0 4 1 2 13 12
		f 4 4 7 -25 -6
		mu 0 4 2 3 14 13
		f 4 6 9 -27 -8
		mu 0 4 3 4 15 14
		f 6 -29 -10 8 10 12 -31
		mu 0 6 16 15 4 5 6 17
		f 4 11 14 -32 -13
		mu 0 4 6 7 18 17
		f 4 13 16 -34 -15
		mu 0 4 7 8 19 18
		f 4 15 18 -36 -17
		mu 0 4 8 9 20 19
		f 4 17 19 -38 -19
		mu 0 4 9 10 21 20
		f 4 20 23 -41 -22
		mu 0 4 11 12 23 22
		f 4 22 25 -43 -24
		mu 0 4 12 13 24 23
		f 4 24 27 -45 -26
		mu 0 4 13 14 25 24
		f 4 26 29 -47 -28
		mu 0 4 14 15 26 25
		f 6 -49 -30 28 30 32 -51
		mu 0 6 27 26 15 16 17 28
		f 4 31 34 -52 -33
		mu 0 4 17 18 29 28
		f 4 33 36 -54 -35
		mu 0 4 18 19 30 29
		f 4 35 38 -56 -37
		mu 0 4 19 20 31 30
		f 4 37 39 -58 -39
		mu 0 4 20 21 32 31
		f 4 40 43 -61 -42
		mu 0 4 22 23 34 33
		f 4 42 45 -63 -44
		mu 0 4 23 24 35 34
		f 4 44 47 -65 -46
		mu 0 4 24 25 36 35
		f 4 46 49 -67 -48
		mu 0 4 25 26 37 36
		f 6 -69 -50 48 50 52 -71
		mu 0 6 38 37 26 27 28 39
		f 4 51 54 -72 -53
		mu 0 4 28 29 40 39
		f 4 53 56 -74 -55
		mu 0 4 29 30 41 40
		f 4 55 58 -76 -57
		mu 0 4 30 31 42 41
		f 4 57 59 -78 -59
		mu 0 4 31 32 43 42
		f 4 60 63 -81 -62
		mu 0 4 33 34 45 44
		f 4 62 65 -82 -64
		mu 0 4 34 35 46 45
		f 4 64 67 -83 -66
		mu 0 4 35 36 47 46
		f 4 66 69 -84 -68
		mu 0 4 36 37 48 47
		f 6 -85 -70 68 70 72 -86
		mu 0 6 49 48 37 38 39 50
		f 4 71 74 -87 -73
		mu 0 4 39 40 51 50
		f 4 73 76 -88 -75
		mu 0 4 40 41 52 51
		f 4 75 78 -89 -77
		mu 0 4 41 42 53 52
		f 4 77 79 -90 -79
		mu 0 4 42 43 54 53;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPlane3" -p "group1";
	setAttr ".t" -type "double3" 0 -0.016389332107191734 0 ;
	setAttr ".s" -type "double3" 24.012908224870134 24.012908224870134 24.012908224870134 ;
createNode mesh -n "pPlaneShape3" -p "pPlane3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".pt";
	setAttr ".pt[162]" -type "float3" 0 -0.0019202139 0 ;
	setAttr ".pt[163]" -type "float3" 0 -0.0036921904 0 ;
	setAttr ".pt[164]" -type "float3" 0 -0.0019202139 0 ;
	setAttr ".pt[182]" -type "float3" 0 -0.00187446 0 ;
	setAttr ".pt[183]" -type "float3" 0 -0.0094747404 0 ;
	setAttr ".pt[184]" -type "float3" 0 -0.013747371 0 ;
	setAttr ".pt[185]" -type "float3" 0 -0.0094747404 0 ;
	setAttr ".pt[186]" -type "float3" 0 -0.00187446 0 ;
	setAttr ".pt[203]" -type "float3" 0 -0.0036099292 0 ;
	setAttr ".pt[204]" -type "float3" 0 -0.013724673 0 ;
	setAttr ".pt[205]" -type "float3" 0 -0.019110302 0 ;
	setAttr ".pt[206]" -type "float3" 0 -0.013724673 0 ;
	setAttr ".pt[207]" -type "float3" 0 -0.0036099292 0 ;
	setAttr ".pt[224]" -type "float3" 0 -0.00187446 0 ;
	setAttr ".pt[225]" -type "float3" 0 -0.0094239917 0 ;
	setAttr ".pt[226]" -type "float3" 0 -0.013724673 0 ;
	setAttr ".pt[227]" -type "float3" 0 -0.0094239917 0 ;
	setAttr ".pt[228]" -type "float3" 0 -0.00187446 0 ;
	setAttr ".pt[246]" -type "float3" 0 -0.00187446 0 ;
	setAttr ".pt[247]" -type "float3" 0 -0.0036275187 0 ;
	setAttr ".pt[248]" -type "float3" 0 -0.00187446 0 ;
createNode transform -n "pPlane4" -p "group1";
	setAttr ".t" -type "double3" 9.0031092263284247 0.05038781798375358 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999972 0 ;
	setAttr ".s" -type "double3" 6 1 -24.872332377330011 ;
	setAttr ".rp" -type "double3" -0.0017715096473660779 0.11484603537246585 2.9357315153986345 ;
	setAttr ".rpt" -type "double3" 2.9375030250460008 0 -2.93396000575125 ;
	setAttr ".sp" -type "double3" -0.00029525160789434674 0.11484603537246585 -0.49765313476529954 ;
	setAttr ".spt" -type "double3" -0.0014762580394717326 0 3.4333846501639442 ;
createNode mesh -n "pPlaneShape4" -p "pPlane4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 66 ".uvst[0].uvsp[0:65]" -type "float2" 0 0.60000002 0.1
		 0.60000002 0.2 0.60000002 0.30000001 0.60000002 0 0.69999999 0.1 0.69999999 0.2 0.69999999
		 0.30000001 0.69999999 0.40000001 0.69999999 0.5 0.69999999 0.60000002 0.69999999
		 0.69999999 0.69999999 0.80000001 0.69999999 0.90000004 0.69999999 1 0.69999999 0
		 0.80000001 0.1 0.80000001 0.2 0.80000001 0.30000001 0.80000001 0.40000001 0.80000001
		 0.5 0.80000001 0.60000002 0.80000001 0.69999999 0.80000001 0.80000001 0.80000001
		 0.90000004 0.80000001 1 0.80000001 0 0.90000004 0.1 0.90000004 0.2 0.90000004 0.30000001
		 0.90000004 0.40000001 0.90000004 0.5 0.90000004 0.60000002 0.90000004 0.69999999
		 0.90000004 0.80000001 0.90000004 0.90000004 0.90000004 1 0.90000004 0 1 0.1 1 0.2
		 1 0.30000001 1 0.40000001 1 0.5 1 0.60000002 1 0.69999999 1 0.80000001 1 0.90000004
		 1 1 1 0.90000004 0.64999998 0.1 0.64999998 0 0.65000033 0.2 0.64999998 0.30000001
		 0.64999998 0.40000001 0.64999998 0.60000002 0.64999998 0.69999999 0.64999998 0.80000001
		 0.64999998 1 0.64999998 0.1 0.625 0 0.62500018 0.2 0.62500042 0.30000001 0.625 0.30000001
		 0.63749999 0.2 0.63752449 0.1 0.63747948 0 0.63750029;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".pt";
	setAttr ".pt[6]" -type "float3" 0 0.00089843606 0 ;
	setAttr ".pt[7]" -type "float3" 0 0.005771236 0 ;
	setAttr ".pt[8]" -type "float3" 0 0.2409716 0 ;
	setAttr ".pt[9]" -type "float3" 0 0.043799672 0 ;
	setAttr ".pt[20]" -type "float3" -9.1163071e-018 0.078570887 0 ;
	setAttr ".pt[21]" -type "float3" -2.7755576e-017 0.15947834 0 ;
	setAttr -s 66 ".vt[0:65]"  -0.5 2.2204466e-017 0.10650647 -0.48649997 2.2204466e-017 0.10650647
		 -0.47015691 2.2204466e-017 0.10650647 -0.45389089 2.2204466e-017 0.10650647 -0.5 4.4408918e-017 -0.19999999
		 -0.48649997 0.17298752 -0.19999999 -0.47015691 0.10262504 -0.19999999 -0.45389089 4.4408918e-017 -0.19999999
		 -0.20153376 4.4408918e-017 -0.19999999 0 4.4408918e-017 -0.19999999 0.14133637 4.4408918e-017 -0.19999999
		 0.44321984 4.4408918e-017 -0.19999999 0.45991737 0.15071905 -0.19999999 0.48127407 0.15071905 -0.19999999
		 0.5 4.4408918e-017 -0.19999999 -0.5 6.6613384e-017 -0.30000001 -0.48649997 0.044878695 -0.30000001
		 -0.47015691 0.12231361 -0.30000001 -0.45389089 6.6613384e-017 -0.30000001 -0.20153376 6.6613384e-017 -0.30000001
		 0 6.6613384e-017 -0.30000001 0.14133637 6.6613384e-017 -0.30000001 0.44321984 6.6613384e-017 -0.30000001
		 0.45991737 0.063624635 -0.30000001 0.48127407 0.10479501 -0.30000001 0.5 6.6613384e-017 -0.30000001
		 -0.5 8.881785e-017 -0.40000004 -0.48649997 0.078291558 -0.40000004 -0.47015691 0.050722033 -0.40000004
		 -0.45389089 8.881785e-017 -0.40000004 -0.20153376 8.881785e-017 -0.40000004 0 8.881785e-017 -0.40000004
		 0.14133637 8.881785e-017 -0.40000004 0.44321984 8.881785e-017 -0.40000004 0.45991737 0.083634213 -0.40000004
		 0.48127407 0.20681684 -0.40000004 0.5 8.881785e-017 -0.40000004 -0.5 1.110223e-016 -0.5
		 -0.48649997 1.110223e-016 -0.5 -0.47015691 1.110223e-016 -0.5 -0.45389089 1.110223e-016 -0.5
		 -0.20153376 1.110223e-016 -0.5 0 1.110223e-016 -0.5 0.14133637 1.110223e-016 -0.5
		 0.44321984 1.110223e-016 -0.5 0.45991737 1.110223e-016 -0.5 0.48127407 1.110223e-016 -0.5
		 0.5 1.110223e-016 -0.5 0.48127407 3.3306692e-017 -0.1244503 -0.48649997 0.086603902 -0.1244503
		 -0.5 3.3306732e-017 -0.12445045 -0.47015691 0.027656253 -0.1244503 -0.45389089 3.3306689e-017 -0.1244503
		 -0.20153376 3.3306692e-017 -0.1244503 0.14133637 3.3306689e-017 -0.1244503 0.44321984 3.3306689e-017 -0.1244503
		 0.45991737 3.3306689e-017 -0.1244503 0.5 3.3306692e-017 -0.1244503 -0.5005905 -0.011279524 0.026521832
		 -0.48709047 0.14309716 0.026521906 -0.47015691 0.070162944 0.0266921 -0.45389089 2.7755576e-017 0.026693985
		 -0.45389089 3.0531131e-017 -0.048878159 -0.47015691 0.080825627 -0.049026117 -0.48679569 0.018160388 -0.048840404
		 -0.50029528 -0.0056397622 -0.04896431;
	setAttr -s 110 ".ed[0:109]"  0 1 0 1 2 0 2 3 0 4 15 0 14 25 0 15 16 1
		 15 26 0 16 17 1 16 27 1 17 18 1 17 28 1 18 19 1 18 29 1 19 20 1 19 30 1 20 21 1 21 22 1
		 21 32 1 22 23 1 22 33 1 23 24 1 23 34 1 24 25 1 24 35 1 25 36 0 26 27 1 26 37 0 27 28 1
		 27 38 1 28 29 1 28 39 1 29 30 1 29 40 1 30 31 1 30 41 1 31 32 1 32 33 1 32 43 1 33 34 1
		 33 44 1 34 35 1 34 45 1 35 36 1 35 46 1 36 47 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 4 50 0 8 53 1 10 54 1 11 55 1 12 56 1 13 48 1
		 4 5 1 5 16 1 5 6 1 6 17 1 6 7 1 7 18 1 7 8 1 8 19 1 8 9 1 9 10 1 10 21 1 10 11 1
		 11 22 1 11 12 1 12 23 1 12 13 1 13 24 1 13 14 1 57 14 0 48 57 0 48 56 0 55 56 0 54 55 0
		 53 54 0 52 53 0 3 61 0 58 0 0 1 59 1 2 60 1 7 52 1 6 51 1 51 52 1 5 49 1 49 51 1
		 60 61 1 61 62 0 51 63 1 59 60 1 60 63 1 49 64 1 58 59 1 59 64 1 49 50 1 50 65 0 62 52 0
		 65 58 0 62 63 1 63 64 1 64 65 1;
	setAttr -s 45 -ch 187 ".fc[0:44]" -type "polyFaces" 
		f 4 5 8 -26 -7
		mu 0 4 15 16 27 26
		f 4 7 10 -28 -9
		mu 0 4 16 17 28 27
		f 4 9 12 -30 -11
		mu 0 4 17 18 29 28
		f 4 11 14 -32 -13
		mu 0 4 18 19 30 29
		f 6 -34 -15 13 15 17 -36
		mu 0 6 31 30 19 20 21 32
		f 4 16 19 -37 -18
		mu 0 4 21 22 33 32
		f 4 18 21 -39 -20
		mu 0 4 22 23 34 33
		f 4 20 23 -41 -22
		mu 0 4 23 24 35 34
		f 4 22 24 -43 -24
		mu 0 4 24 25 36 35
		f 4 25 28 -46 -27
		mu 0 4 26 27 38 37
		f 4 27 30 -47 -29
		mu 0 4 27 28 39 38
		f 4 29 32 -48 -31
		mu 0 4 28 29 40 39
		f 4 31 34 -49 -33
		mu 0 4 29 30 41 40
		f 6 -50 -35 33 35 37 -51
		mu 0 6 42 41 30 31 32 43
		f 4 36 39 -52 -38
		mu 0 4 32 33 44 43
		f 4 38 41 -53 -40
		mu 0 4 33 34 45 44
		f 4 40 43 -54 -42
		mu 0 4 34 35 46 45
		f 4 42 44 -55 -44
		mu 0 4 35 36 47 46
		f 4 -4 61 62 -6
		mu 0 4 15 4 5 16
		f 4 -63 63 64 -8
		mu 0 4 16 5 6 17
		f 4 -65 65 66 -10
		mu 0 4 17 6 7 18
		f 4 -67 67 68 -12
		mu 0 4 18 7 8 19
		f 6 -14 -69 69 70 71 -16
		mu 0 6 20 19 8 9 10 21
		f 4 -72 72 73 -17
		mu 0 4 21 10 11 22
		f 4 -74 74 75 -19
		mu 0 4 22 11 12 23
		f 4 -76 76 77 -21
		mu 0 4 23 12 13 24
		f 4 -78 78 4 -23
		mu 0 4 24 13 14 25
		f 4 80 79 -79 60
		mu 0 4 48 57 14 13
		f 4 -82 -61 -77 59
		mu 0 4 56 48 13 12
		f 4 82 -60 -75 58
		mu 0 4 55 56 12 11
		f 4 83 -59 -73 57
		mu 0 4 54 55 11 10
		f 5 84 -58 -71 -70 56
		mu 0 5 53 54 10 9 8
		f 4 -102 87 0 88
		mu 0 4 58 59 0 1
		f 4 -99 -89 1 89
		mu 0 4 60 58 1 2
		f 4 86 -96 -90 2
		mu 0 4 3 61 60 2
		f 4 -68 90 85 -57
		mu 0 4 8 7 52 53
		f 4 -66 91 92 -91
		mu 0 4 7 6 51 52
		f 4 -64 93 94 -92
		mu 0 4 6 5 49 51
		f 4 -62 55 -104 -94
		mu 0 4 5 4 50 49
		f 4 107 -100 95 96
		mu 0 4 62 63 60 61
		f 4 108 -103 98 99
		mu 0 4 63 64 58 60
		f 4 109 106 101 102
		mu 0 4 64 65 59 58
		f 4 -108 105 -93 97
		mu 0 4 63 62 52 51
		f 4 -109 -98 -95 100
		mu 0 4 64 63 51 49
		f 4 -110 -101 103 104
		mu 0 4 65 64 49 50;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape1" -p "pPlane4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 55 ".uvst[0].uvsp[0:54]" -type "float2" 0 0.60000002 0.1
		 0.60000002 0.2 0.60000002 0.30000001 0.60000002 0.40000001 0.60000002 0.5 0.60000002
		 0.60000002 0.60000002 0.69999999 0.60000002 0.80000001 0.60000002 0.90000004 0.60000002
		 1 0.60000002 0 0.69999999 0.1 0.69999999 0.2 0.69999999 0.30000001 0.69999999 0.40000001
		 0.69999999 0.5 0.69999999 0.60000002 0.69999999 0.69999999 0.69999999 0.80000001
		 0.69999999 0.90000004 0.69999999 1 0.69999999 0 0.80000001 0.1 0.80000001 0.2 0.80000001
		 0.30000001 0.80000001 0.40000001 0.80000001 0.5 0.80000001 0.60000002 0.80000001
		 0.69999999 0.80000001 0.80000001 0.80000001 0.90000004 0.80000001 1 0.80000001 0
		 0.90000004 0.1 0.90000004 0.2 0.90000004 0.30000001 0.90000004 0.40000001 0.90000004
		 0.5 0.90000004 0.60000002 0.90000004 0.69999999 0.90000004 0.80000001 0.90000004
		 0.90000004 0.90000004 1 0.90000004 0 1 0.1 1 0.2 1 0.30000001 1 0.40000001 1 0.5
		 1 0.60000002 1 0.69999999 1 0.80000001 1 0.90000004 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 -3.7252903e-009 ;
	setAttr ".pt[1]" -type "float3" 0 0 -3.7252903e-009 ;
	setAttr ".pt[2]" -type "float3" 0 0 -3.7252903e-009 ;
	setAttr ".pt[3]" -type "float3" 0 0 -3.7252903e-009 ;
	setAttr -s 55 ".vt[0:54]"  -0.5 2.2204466e-017 -0.12455679 -0.48649997 2.2204466e-017 -0.12455679
		 -0.47015691 2.2204466e-017 -0.12455679 -0.45389089 2.2204466e-017 -0.12455679 -0.20153376 2.2204466e-017 -0.12455679
		 0 2.2204466e-017 -0.12455679 0.14133637 2.2204466e-017 -0.12455679 0.44321984 2.2204466e-017 -0.12455679
		 0.45991737 2.2204466e-017 -0.12455679 0.48127407 2.2204466e-017 -0.12455679 0.5 2.2204466e-017 -0.12455679
		 -0.5 4.4408918e-017 -0.19999999 -0.48649997 4.4408918e-017 -0.19999999 -0.47015691 4.4408918e-017 -0.19999999
		 -0.45389089 4.4408918e-017 -0.19999999 -0.20153376 4.4408918e-017 -0.19999999 0 4.4408918e-017 -0.19999999
		 0.14133637 4.4408918e-017 -0.19999999 0.44321984 4.4408918e-017 -0.19999999 0.45991737 4.4408918e-017 -0.19999999
		 0.48127407 4.4408918e-017 -0.19999999 0.5 4.4408918e-017 -0.19999999 -0.5 6.6613384e-017 -0.30000001
		 -0.48649997 6.6613384e-017 -0.30000001 -0.47015691 6.6613384e-017 -0.30000001 -0.45389089 6.6613384e-017 -0.30000001
		 -0.20153376 6.6613384e-017 -0.30000001 0 6.6613384e-017 -0.30000001 0.14133637 6.6613384e-017 -0.30000001
		 0.44321984 6.6613384e-017 -0.30000001 0.45991737 6.6613384e-017 -0.30000001 0.48127407 6.6613384e-017 -0.30000001
		 0.5 6.6613384e-017 -0.30000001 -0.5 8.881785e-017 -0.40000004 -0.48649997 8.881785e-017 -0.40000004
		 -0.47015691 8.881785e-017 -0.40000004 -0.45389089 8.881785e-017 -0.40000004 -0.20153376 8.881785e-017 -0.40000004
		 0 8.881785e-017 -0.40000004 0.14133637 8.881785e-017 -0.40000004 0.44321984 8.881785e-017 -0.40000004
		 0.45991737 8.881785e-017 -0.40000004 0.48127407 8.881785e-017 -0.40000004 0.5 8.881785e-017 -0.40000004
		 -0.5 1.110223e-016 -0.5 -0.48649997 1.110223e-016 -0.5 -0.47015691 1.110223e-016 -0.5
		 -0.45389089 1.110223e-016 -0.5 -0.20153376 1.110223e-016 -0.5 0 1.110223e-016 -0.5
		 0.14133637 1.110223e-016 -0.5 0.44321984 1.110223e-016 -0.5 0.45991737 1.110223e-016 -0.5
		 0.48127407 1.110223e-016 -0.5 0.5 1.110223e-016 -0.5;
	setAttr -s 90 ".ed[0:89]"  0 1 0 0 11 0 1 2 0 1 12 1 2 3 0 2 13 1 3 4 0
		 3 14 1 4 5 0 4 15 1 5 6 0 6 7 0 6 17 1 7 8 0 7 18 1 8 9 0 8 19 1 9 10 0 9 20 1 10 21 0
		 11 12 1 11 22 0 12 13 1 12 23 1 13 14 1 13 24 1 14 15 1 14 25 1 15 16 1 15 26 1 16 17 1
		 17 18 1 17 28 1 18 19 1 18 29 1 19 20 1 19 30 1 20 21 1 20 31 1 21 32 0 22 23 1 22 33 0
		 23 24 1 23 34 1 24 25 1 24 35 1 25 26 1 25 36 1 26 27 1 26 37 1 27 28 1 28 29 1 28 39 1
		 29 30 1 29 40 1 30 31 1 30 41 1 31 32 1 31 42 1 32 43 0 33 34 1 33 44 0 34 35 1 34 45 1
		 35 36 1 35 46 1 36 37 1 36 47 1 37 38 1 37 48 1 38 39 1 39 40 1 39 50 1 40 41 1 40 51 1
		 41 42 1 41 52 1 42 43 1 42 53 1 43 54 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0
		 50 51 0 51 52 0 52 53 0 53 54 0;
	setAttr -s 36 -ch 152 ".fc[0:35]" -type "polyFaces" 
		f 4 0 3 -21 -2
		mu 0 4 0 1 12 11
		f 4 2 5 -23 -4
		mu 0 4 1 2 13 12
		f 4 4 7 -25 -6
		mu 0 4 2 3 14 13
		f 4 6 9 -27 -8
		mu 0 4 3 4 15 14
		f 6 -29 -10 8 10 12 -31
		mu 0 6 16 15 4 5 6 17
		f 4 11 14 -32 -13
		mu 0 4 6 7 18 17
		f 4 13 16 -34 -15
		mu 0 4 7 8 19 18
		f 4 15 18 -36 -17
		mu 0 4 8 9 20 19
		f 4 17 19 -38 -19
		mu 0 4 9 10 21 20
		f 4 20 23 -41 -22
		mu 0 4 11 12 23 22
		f 4 22 25 -43 -24
		mu 0 4 12 13 24 23
		f 4 24 27 -45 -26
		mu 0 4 13 14 25 24
		f 4 26 29 -47 -28
		mu 0 4 14 15 26 25
		f 6 -49 -30 28 30 32 -51
		mu 0 6 27 26 15 16 17 28
		f 4 31 34 -52 -33
		mu 0 4 17 18 29 28
		f 4 33 36 -54 -35
		mu 0 4 18 19 30 29
		f 4 35 38 -56 -37
		mu 0 4 19 20 31 30
		f 4 37 39 -58 -39
		mu 0 4 20 21 32 31
		f 4 40 43 -61 -42
		mu 0 4 22 23 34 33
		f 4 42 45 -63 -44
		mu 0 4 23 24 35 34
		f 4 44 47 -65 -46
		mu 0 4 24 25 36 35
		f 4 46 49 -67 -48
		mu 0 4 25 26 37 36
		f 6 -69 -50 48 50 52 -71
		mu 0 6 38 37 26 27 28 39
		f 4 51 54 -72 -53
		mu 0 4 28 29 40 39
		f 4 53 56 -74 -55
		mu 0 4 29 30 41 40
		f 4 55 58 -76 -57
		mu 0 4 30 31 42 41
		f 4 57 59 -78 -59
		mu 0 4 31 32 43 42
		f 4 60 63 -81 -62
		mu 0 4 33 34 45 44
		f 4 62 65 -82 -64
		mu 0 4 34 35 46 45
		f 4 64 67 -83 -66
		mu 0 4 35 36 47 46
		f 4 66 69 -84 -68
		mu 0 4 36 37 48 47
		f 6 -85 -70 68 70 72 -86
		mu 0 6 49 48 37 38 39 50
		f 4 71 74 -87 -73
		mu 0 4 39 40 51 50
		f 4 73 76 -88 -75
		mu 0 4 40 41 52 51
		f 4 75 78 -89 -77
		mu 0 4 41 42 53 52
		f 4 77 79 -90 -79
		mu 0 4 42 43 54 53;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n"
		+ "                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n"
		+ "            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n"
		+ "            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n"
		+ "            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n"
		+ "                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n"
		+ "                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n"
		+ "                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n"
		+ "                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n"
		+ "                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n"
		+ "                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n"
		+ "                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode polyPlane -n "polyPlane1";
	setAttr ".cuv" 2;
createNode polyTweak -n "polyTweak1";
	setAttr ".uopa" yes;
	setAttr -s 32 ".tk";
	setAttr ".tk[1]" -type "float3" -2.7939677e-009 0 0 ;
	setAttr ".tk[12]" -type "float3" -2.7939677e-009 0 0 ;
	setAttr ".tk[23]" -type "float3" -2.7939677e-009 0 0 ;
	setAttr ".tk[34]" -type "float3" -2.7939677e-009 0 0 ;
	setAttr ".tk[44]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[45]" -type "float3" -2.7939677e-009 0 0.024581771 ;
	setAttr ".tk[46]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[47]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[48]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[49]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[50]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[51]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[52]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[53]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[54]" -type "float3" 0 0 0.024581771 ;
	setAttr ".tk[56]" -type "float3" -2.7939677e-009 0 0 ;
	setAttr ".tk[66]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[67]" -type "float3" -2.7939677e-009 0 -0.024556762 ;
	setAttr ".tk[68]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[69]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[70]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[71]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[72]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[73]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[74]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[75]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[76]" -type "float3" 0 0 -0.024556762 ;
	setAttr ".tk[78]" -type "float3" -2.7939677e-009 0 0 ;
	setAttr ".tk[89]" -type "float3" -2.7939677e-009 0 0 ;
	setAttr ".tk[100]" -type "float3" -2.7939677e-009 0 0 ;
	setAttr ".tk[111]" -type "float3" -2.7939677e-009 0 0 ;
createNode deleteComponent -n "deleteComponent1";
	setAttr ".dc" -type "componentList" 1 "f[0:39]";
createNode polyTweak -n "polyTweak2";
	setAttr ".uopa" yes;
	setAttr -s 52 ".tk";
	setAttr ".tk[1]" -type "float3" -0.086499959 0 0 ;
	setAttr ".tk[2]" -type "float3" -0.1701569 0 0 ;
	setAttr ".tk[3]" -type "float3" -0.2538909 0 0 ;
	setAttr ".tk[6]" -type "float3" 0.038979329 0 0 ;
	setAttr ".tk[7]" -type "float3" 0.24321985 0 0 ;
	setAttr ".tk[8]" -type "float3" 0.15991735 0 0 ;
	setAttr ".tk[9]" -type "float3" 0.081274018 0 0 ;
	setAttr ".tk[12]" -type "float3" -0.086499959 0 0 ;
	setAttr ".tk[13]" -type "float3" -0.1701569 0 0 ;
	setAttr ".tk[14]" -type "float3" -0.2538909 0 0 ;
	setAttr ".tk[17]" -type "float3" 0.038979329 0 0 ;
	setAttr ".tk[18]" -type "float3" 0.24321985 0 0 ;
	setAttr ".tk[19]" -type "float3" 0.15991735 0 0 ;
	setAttr ".tk[20]" -type "float3" 0.081274018 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.086499959 0 0 ;
	setAttr ".tk[24]" -type "float3" -0.1701569 0 0 ;
	setAttr ".tk[25]" -type "float3" -0.2538909 0 0 ;
	setAttr ".tk[28]" -type "float3" 0.038979329 0 0 ;
	setAttr ".tk[29]" -type "float3" 0.24321985 0 0 ;
	setAttr ".tk[30]" -type "float3" 0.15991735 0 0 ;
	setAttr ".tk[31]" -type "float3" 0.081274018 0 0 ;
	setAttr ".tk[34]" -type "float3" -0.086499959 0 0 ;
	setAttr ".tk[35]" -type "float3" -0.1701569 0 0 ;
	setAttr ".tk[36]" -type "float3" -0.2538909 0 0 ;
	setAttr ".tk[39]" -type "float3" 0.038979329 0 0 ;
	setAttr ".tk[40]" -type "float3" 0.24321985 0 0 ;
	setAttr ".tk[41]" -type "float3" 0.15991735 0 0 ;
	setAttr ".tk[42]" -type "float3" 0.081274018 0 0 ;
	setAttr ".tk[45]" -type "float3" -0.086499959 0 0 ;
	setAttr ".tk[46]" -type "float3" -0.1701569 0 0 ;
	setAttr ".tk[47]" -type "float3" -0.2538909 0 0 ;
	setAttr ".tk[50]" -type "float3" 0.038979329 0 0 ;
	setAttr ".tk[51]" -type "float3" 0.24321985 0 0 ;
	setAttr ".tk[52]" -type "float3" 0.15991735 0 0 ;
	setAttr ".tk[53]" -type "float3" 0.081274018 0 0 ;
	setAttr ".tk[56]" -type "float3" -0.086499959 0 0 ;
	setAttr ".tk[57]" -type "float3" -0.1701569 0 0 ;
	setAttr ".tk[58]" -type "float3" -0.2538909 0 0 ;
	setAttr ".tk[61]" -type "float3" 0.038979329 0 0 ;
	setAttr ".tk[62]" -type "float3" 0.24321985 0 0 ;
	setAttr ".tk[63]" -type "float3" 0.15991735 0 0 ;
	setAttr ".tk[64]" -type "float3" 0.081274018 0 0 ;
	setAttr ".tk[67]" -type "float3" -0.086499959 0 0 ;
	setAttr ".tk[68]" -type "float3" -0.1701569 0 0 ;
	setAttr ".tk[69]" -type "float3" -0.2538909 0 0 ;
	setAttr ".tk[72]" -type "float3" 0.038979329 0 0 ;
	setAttr ".tk[73]" -type "float3" 0.24321985 0 0 ;
	setAttr ".tk[74]" -type "float3" 0.15991735 0 0 ;
	setAttr ".tk[75]" -type "float3" 0.081274018 0 0 ;
createNode deleteComponent -n "deleteComponent2";
	setAttr ".dc" -type "componentList" 6 "e[11]" "e[32]" "e[53]" "e[74]" "e[95]" "e[116]";
createNode polyTweak -n "polyTweak3";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[4]" -type "float3" -0.10153377 0 0 ;
	setAttr ".tk[6]" -type "float3" 0.002357017 0 0 ;
	setAttr ".tk[15]" -type "float3" -0.10153377 0 0 ;
	setAttr ".tk[17]" -type "float3" 0.002357017 0 0 ;
	setAttr ".tk[26]" -type "float3" -0.10153377 0 0 ;
	setAttr ".tk[28]" -type "float3" 0.002357017 0 0 ;
	setAttr ".tk[37]" -type "float3" -0.10153377 0 0 ;
	setAttr ".tk[39]" -type "float3" 0.002357017 0 0 ;
	setAttr ".tk[48]" -type "float3" -0.10153377 0 0 ;
	setAttr ".tk[50]" -type "float3" 0.002357017 0 0 ;
	setAttr ".tk[59]" -type "float3" -0.10153377 0 0 ;
	setAttr ".tk[61]" -type "float3" 0.002357017 0 0 ;
	setAttr ".tk[70]" -type "float3" -0.10153377 0 0 ;
	setAttr ".tk[72]" -type "float3" 0.002357017 0 0 ;
createNode deleteComponent -n "deleteComponent3";
	setAttr ".dc" -type "componentList" 2 "f[0:2]" "f[9:11]";
createNode polySplit -n "polySplit1";
	setAttr ".e[0]"  0.49999994;
	setAttr ".d[0]"  -2147483630;
createNode polySplit -n "polySplit2";
	setAttr -s 20 ".sps[0].sp";
	setAttr ".sps[0].sp[0].f" 8;
	setAttr ".sps[0].sp[0].t" 1;
	setAttr ".sps[0].sp[0].bc" -type "double3" 0.5 0.49999988079071045 1.1920928955078125e-007 ;
	setAttr ".sps[0].sp[1].f" 7;
	setAttr ".sps[0].sp[1].t" 2;
	setAttr ".sps[0].sp[1].bc" -type "double3" 1 0 0 ;
	setAttr ".sps[0].sp[2].f" 7;
	setAttr ".sps[0].sp[2].t" 1;
	setAttr ".sps[0].sp[2].bc" -type "double3" 0.5000007152557373 0 0.4999992847442627 ;
	setAttr ".sps[0].sp[3].f" 6;
	setAttr ".sps[0].sp[3].t" 1;
	setAttr ".sps[0].sp[3].bc" -type "double3" 0 0.50000011920928955 0.49999988079071045 ;
	setAttr ".sps[0].sp[4].f" 6;
	setAttr ".sps[0].sp[4].t" 1;
	setAttr ".sps[0].sp[4].bc" -type "double3" 0.49999994039535522 0.50000005960464478 
		0 ;
	setAttr ".sps[0].sp[5].f" 5;
	setAttr ".sps[0].sp[5].t" 1;
	setAttr ".sps[0].sp[5].bc" -type "double3" 0 0.50000011920928955 0.49999988079071045 ;
	setAttr ".sps[0].sp[6].f" 5;
	setAttr ".sps[0].sp[6].t" 1;
	setAttr ".sps[0].sp[6].bc" -type "double3" 0.49999994039535522 0.50000005960464478 
		0 ;
	setAttr ".sps[0].sp[7].f" 4;
	setAttr ".sps[0].sp[7].t" 3;
	setAttr ".sps[0].sp[7].bc" -type "double3" 0 0.50000011920928955 0.49999988079071045 ;
	setAttr ".sps[0].sp[8].f" 4;
	setAttr ".sps[0].sp[8].t" 3;
	setAttr ".sps[0].sp[8].bc" -type "double3" 0.49999994039535522 1.9198326128844201e-007 
		0.49999988079071045 ;
	setAttr ".sps[0].sp[9].f" 4;
	setAttr ".sps[0].sp[9].t" 2;
	setAttr ".sps[0].sp[9].bc" -type "double3" 0.50000011920928955 0.49999988079071045 
		0 ;
	setAttr ".sps[0].sp[10].f" 4;
	setAttr ".sps[0].sp[10].t" 1;
	setAttr ".sps[0].sp[10].bc" -type "double3" 0.49999997019767761 0 0.5 ;
	setAttr ".sps[0].sp[11].f" 3;
	setAttr ".sps[0].sp[11].t" 1;
	setAttr ".sps[0].sp[11].bc" -type "double3" 0 0.50000005960464478 0.49999994039535522 ;
	setAttr ".sps[0].sp[12].f" 3;
	setAttr ".sps[0].sp[12].bc" -type "double3" 4.0146609592284221e-008 0.5 0.49999994039535522 ;
	setAttr ".sps[0].sp[13].f" 2;
	setAttr ".sps[0].sp[13].t" 1;
	setAttr ".sps[0].sp[13].bc" -type "double3" 0 0.50000011920928955 0.49999988079071045 ;
	setAttr ".sps[0].sp[14].f" 2;
	setAttr ".sps[0].sp[14].t" 1;
	setAttr ".sps[0].sp[14].bc" -type "double3" 0.49999910593032837 0.50000011920928955 
		7.7486038208007813e-007 ;
	setAttr ".sps[0].sp[15].f" 1;
	setAttr ".sps[0].sp[15].t" 1;
	setAttr ".sps[0].sp[15].bc" -type "double3" 0 0.50000005960464478 0.49999994039535522 ;
	setAttr ".sps[0].sp[16].f" 1;
	setAttr ".sps[0].sp[16].bc" -type "double3" 0 0.5 0.5 ;
	setAttr ".sps[0].sp[17].t" 1;
	setAttr ".sps[0].sp[17].bc" -type "double3" 0 0.50000011920928955 0.49999988079071045 ;
	setAttr ".sps[0].sp[18].t" 1;
	setAttr ".sps[0].sp[18].bc" -type "double3" 0.49999994039535522 0.50000005960464478 
		0 ;
	setAttr ".sps[0].sp[19].bc" -type "double3" 0.49999824166297913 0 0.50000178813934326 ;
	setAttr ".c2v" yes;
createNode polyTweak -n "polyTweak4";
	setAttr ".uopa" yes;
	setAttr -s 21 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[1]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[2]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[3]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[4]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[5]" -type "float3" 4.2263762e-016 0 0.23106326 ;
	setAttr ".tk[6]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[7]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[8]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[9]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[10]" -type "float3" 0 0 0.23106326 ;
	setAttr ".tk[55]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[56]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[57]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[58]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[59]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[60]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[61]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[62]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[63]" -type "float3" 0 0 0.039826114 ;
	setAttr ".tk[64]" -type "float3" 0 0 0.039826114 ;
createNode deleteComponent -n "deleteComponent4";
	setAttr ".dc" -type "componentList" 1 "f[26]";
createNode deleteComponent -n "deleteComponent5";
	setAttr ".dc" -type "componentList" 1 "f[25]";
createNode deleteComponent -n "deleteComponent6";
	setAttr ".dc" -type "componentList" 1 "f[24]";
createNode deleteComponent -n "deleteComponent7";
	setAttr ".dc" -type "componentList" 1 "f[22]";
createNode deleteComponent -n "deleteComponent8";
	setAttr ".dc" -type "componentList" 1 "f[21]";
createNode polyTweak -n "polyTweak5";
	setAttr ".uopa" yes;
	setAttr -s 15 ".tk";
	setAttr ".tk[50]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[51]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[52]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[53]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[54]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[55]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[56]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[57]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[58]" -type "float3" 0 0 -0.001998029 ;
	setAttr ".tk[59]" -type "float3" 0 0 -0.001998029 ;
createNode deleteComponent -n "deleteComponent9";
	setAttr ".dc" -type "componentList" 1 "f[21]";
createNode polySplit -n "polySplit3";
	setAttr -s 7 ".sps[0].sp";
	setAttr ".sps[0].sp[0].f" 20;
	setAttr ".sps[0].sp[0].bc" -type "double3" 0.5 0 0.5 ;
	setAttr ".sps[0].sp[1].f" 20;
	setAttr ".sps[0].sp[1].t" 1;
	setAttr ".sps[0].sp[1].bc" -type "double3" 0.49999907612800598 0.50000011920928955 
		8.3446502685546875e-007 ;
	setAttr ".sps[0].sp[2].f" 19;
	setAttr ".sps[0].sp[2].bc" -type "double3" 0.50000816583633423 0 0.49999183416366577 ;
	setAttr ".sps[0].sp[3].f" 19;
	setAttr ".sps[0].sp[3].t" 1;
	setAttr ".sps[0].sp[3].bc" -type "double3" 0.49999988079071045 0.50000011920928955 
		0 ;
	setAttr ".sps[0].sp[4].f" 18;
	setAttr ".sps[0].sp[4].bc" -type "double3" 0.5 0 0.5 ;
	setAttr ".sps[0].sp[5].f" 18;
	setAttr ".sps[0].sp[5].t" 1;
	setAttr ".sps[0].sp[5].bc" -type "double3" 0.5 0.49999994039535522 5.9604644775390625e-008 ;
	setAttr ".sps[0].sp[6].f" 18;
	setAttr ".sps[0].sp[6].t" 1;
	setAttr ".sps[0].sp[6].bc" -type "double3" 0 0.5 0.5 ;
	setAttr ".c2v" yes;
createNode polySplit -n "polySplit4";
	setAttr -s 7 ".sps[0].sp";
	setAttr ".sps[0].sp[0].f" 39;
	setAttr ".sps[0].sp[0].t" 1;
	setAttr ".sps[0].sp[0].bc" -type "double3" 0 0.5 0.5 ;
	setAttr ".sps[0].sp[1].f" 39;
	setAttr ".sps[0].sp[1].t" 1;
	setAttr ".sps[0].sp[1].bc" -type "double3" 0.5004919171333313 0.49950754642486572 
		5.3644180297851563e-007 ;
	setAttr ".sps[0].sp[2].f" 39;
	setAttr ".sps[0].sp[2].bc" -type "double3" 0.4990273118019104 0 0.5009726881980896 ;
	setAttr ".sps[0].sp[3].f" 40;
	setAttr ".sps[0].sp[3].t" 1;
	setAttr ".sps[0].sp[3].bc" -type "double3" 0.50147342681884766 0.49852654337882996 
		2.9802322387695313e-008 ;
	setAttr ".sps[0].sp[4].f" 40;
	setAttr ".sps[0].sp[4].bc" -type "double3" 0.50081998109817505 0 0.49918001890182495 ;
	setAttr ".sps[0].sp[5].f" 41;
	setAttr ".sps[0].sp[5].t" 1;
	setAttr ".sps[0].sp[5].bc" -type "double3" 0.49958908557891846 0.50041085481643677 
		5.9604644775390625e-008 ;
	setAttr ".sps[0].sp[6].f" 41;
	setAttr ".sps[0].sp[6].bc" -type "double3" 0.5 0 0.5 ;
	setAttr ".c2v" yes;
createNode polyTweak -n "polyTweak6";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[58]" -type "float3" -0.00059051695 -0.011279524 0.035493825 ;
	setAttr ".tk[59]" -type "float3" -0.00059051695 -0.011279524 0.035493825 ;
	setAttr ".tk[60]" -type "float3" 0 0 0.035665903 ;
	setAttr ".tk[61]" -type "float3" 0 0 0.035665903 ;
createNode polySoftEdge -n "polySoftEdge1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 6 0 0 0 0 1 0 0 0 0 24 0 0 0.05038781798375358 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak7";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[13]" -type "float3" 0 0.10434761 0 ;
	setAttr ".tk[14]" -type "float3" 0 0.20921014 0 ;
	setAttr ".tk[16]" -type "float3" 0 7.4505806e-009 0 ;
	setAttr ".tk[17]" -type "float3" 0 -0.0033319527 0.0054042749 ;
	setAttr ".tk[18]" -type "float3" 0 -0.0033319527 0.0054042749 ;
	setAttr ".tk[24]" -type "float3" 0 0.15657903 0 ;
	setAttr ".tk[25]" -type "float3" 0 0.087330997 0 ;
	setAttr ".tk[28]" -type "float3" 0 0.28369886 0 ;
	setAttr ".tk[29]" -type "float3" 0 0.2836988 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.041439082 0 ;
	setAttr ".tk[36]" -type "float3" 0 0.099359363 0 ;
	setAttr ".tk[39]" -type "float3" 0 0.035137229 0 ;
	setAttr ".tk[40]" -type "float3" 0 0.080333501 0 ;
	setAttr ".tk[46]" -type "float3" 0 0.26263481 0 ;
	setAttr ".tk[47]" -type "float3" 0 0.31454208 0 ;
	setAttr ".tk[50]" -type "float3" 0 0.06269379 0 ;
	setAttr ".tk[51]" -type "float3" 0 0.14054309 0 ;
	setAttr ".tk[57]" -type "float3" 0 0.33519301 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.24368487 0 ;
createNode polySoftEdge -n "polySoftEdge2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 2.6645352591003757e-015 0 -6 0 0 1 0 0 24.712584419590854 0 1.0974592088249071e-014 0
		 0.35734121932970986 0.05038781798375358 1.5869137973899351e-016 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak8";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[5]" -type "float3" 0 0.17298752 0 ;
	setAttr ".tk[6]" -type "float3" 0 0.10262504 0 ;
	setAttr ".tk[12]" -type "float3" 0 0.15071905 0 ;
	setAttr ".tk[13]" -type "float3" 0 0.15071905 0 ;
	setAttr ".tk[16]" -type "float3" 0 0.044878695 0 ;
	setAttr ".tk[17]" -type "float3" 0 0.12231361 0 ;
	setAttr ".tk[23]" -type "float3" 0 0.063624635 0 ;
	setAttr ".tk[24]" -type "float3" 0 0.10479501 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.078291558 0 ;
	setAttr ".tk[28]" -type "float3" 0 0.050722033 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.083634213 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.20681684 0 ;
	setAttr ".tk[49]" -type "float3" 0 0.086603902 0 ;
	setAttr ".tk[51]" -type "float3" 0 0.027656253 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.15437669 0 ;
	setAttr ".tk[60]" -type "float3" 0 0.070162944 0 ;
	setAttr ".tk[63]" -type "float3" 0 0.080825627 0 ;
	setAttr ".tk[64]" -type "float3" 0 0.023809399 0 ;
createNode polyPlane -n "polyPlane2";
	setAttr ".sw" 20;
	setAttr ".sh" 20;
	setAttr ".cuv" 2;
createNode polySoftEdge -n "polySoftEdge3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 24.012908224870134 0 0 0 0 24.012908224870134 0 0 0 0 24.012908224870134 0
		 0 -0.016389332107191734 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak9";
	setAttr ".uopa" yes;
	setAttr -s 153 ".tk";
	setAttr ".tk[4]" -type "float3" 0 0.0041234526 0 ;
	setAttr ".tk[5]" -type "float3" 0 0.0078880349 0 ;
	setAttr ".tk[6]" -type "float3" 0 0.0041234526 0 ;
	setAttr ".tk[24]" -type "float3" 0 0.0041234526 0 ;
	setAttr ".tk[25]" -type "float3" 0 0.020269848 0 ;
	setAttr ".tk[26]" -type "float3" 0 0.029386256 0 ;
	setAttr ".tk[27]" -type "float3" 0 0.020269848 0 ;
	setAttr ".tk[28]" -type "float3" 0 0.0041234526 0 ;
	setAttr ".tk[45]" -type "float3" 0 0.0078880349 0 ;
	setAttr ".tk[46]" -type "float3" 0 0.029386256 0 ;
	setAttr ".tk[47]" -type "float3" 0 0.040826906 0 ;
	setAttr ".tk[48]" -type "float3" 0 0.029386256 0 ;
	setAttr ".tk[49]" -type "float3" 0 0.0078880349 0 ;
	setAttr ".tk[53]" -type "float3" 0 0.0024613841 0 ;
	setAttr ".tk[54]" -type "float3" 0 0.0047085504 0 ;
	setAttr ".tk[55]" -type "float3" 0 0.0024613841 0 ;
	setAttr ".tk[57]" -type "float3" 0 0.0082145371 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.015714154 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.0082145371 0 ;
	setAttr ".tk[66]" -type "float3" 0 0.0041234526 0 ;
	setAttr ".tk[67]" -type "float3" 0 0.020269848 0 ;
	setAttr ".tk[68]" -type "float3" 0 0.029386256 0 ;
	setAttr ".tk[69]" -type "float3" 0 0.020269848 0 ;
	setAttr ".tk[70]" -type "float3" 0 0.0041234526 0 ;
	setAttr ".tk[73]" -type "float3" 0 0.0024613841 0 ;
	setAttr ".tk[74]" -type "float3" 0 0.012099531 0 ;
	setAttr ".tk[75]" -type "float3" 0 0.01754133 0 ;
	setAttr ".tk[76]" -type "float3" 0 0.012099531 0 ;
	setAttr ".tk[77]" -type "float3" 0 0.010626766 0 ;
	setAttr ".tk[78]" -type "float3" 0 0.040380564 0 ;
	setAttr ".tk[79]" -type "float3" 0 0.058541819 0 ;
	setAttr ".tk[80]" -type "float3" 0 0.040380564 0 ;
	setAttr ".tk[81]" -type "float3" 0 0.0082145371 0 ;
	setAttr ".tk[88]" -type "float3" 0 0.0041234526 0 ;
	setAttr ".tk[89]" -type "float3" 0 0.0078880349 0 ;
	setAttr ".tk[90]" -type "float3" 0 0.0041234526 0 ;
	setAttr ".tk[91]" -type "float3" 0 0.0014745247 0 ;
	setAttr ".tk[92]" -type "float3" 0 0.0028207214 0 ;
	setAttr ".tk[93]" -type "float3" 0 0.0014745247 0 ;
	setAttr ".tk[94]" -type "float3" 0 0.0047085504 0 ;
	setAttr ".tk[95]" -type "float3" 0 0.01754133 0 ;
	setAttr ".tk[96]" -type "float3" 0 0.024370525 0 ;
	setAttr ".tk[97]" -type "float3" 0 0.01754133 0 ;
	setAttr ".tk[98]" -type "float3" 0 0.020162087 0 ;
	setAttr ".tk[99]" -type "float3" 0 0.058541819 0 ;
	setAttr ".tk[100]" -type "float3" 0 0.081333317 0 ;
	setAttr ".tk[101]" -type "float3" 0 0.058541819 0 ;
	setAttr ".tk[102]" -type "float3" 0 0.015714154 0 ;
	setAttr ".tk[111]" -type "float3" 0 0.0014745247 0 ;
	setAttr ".tk[112]" -type "float3" 0 0.0072483923 0 ;
	setAttr ".tk[113]" -type "float3" 0 0.010508377 0 ;
	setAttr ".tk[114]" -type "float3" 0 0.0072483923 0 ;
	setAttr ".tk[115]" -type "float3" 0 0.0039359089 0 ;
	setAttr ".tk[116]" -type "float3" 0 0.012099531 0 ;
	setAttr ".tk[117]" -type "float3" 0 0.01754133 0 ;
	setAttr ".tk[118]" -type "float3" 0 0.012099531 0 ;
	setAttr ".tk[119]" -type "float3" 0 0.010626766 0 ;
	setAttr ".tk[120]" -type "float3" 0 0.040907566 0 ;
	setAttr ".tk[121]" -type "float3" 0 0.059229299 0 ;
	setAttr ".tk[122]" -type "float3" 0 0.040907566 0 ;
	setAttr ".tk[123]" -type "float3" 0 0.0082145371 0 ;
	setAttr ".tk[132]" -type "float3" 0 0.0028207214 0 ;
	setAttr ".tk[133]" -type "float3" 0 0.010508377 0 ;
	setAttr ".tk[134]" -type "float3" 0 0.014599503 0 ;
	setAttr ".tk[135]" -type "float3" 0 0.010508377 0 ;
	setAttr ".tk[136]" -type "float3" 0 0.0028207214 0 ;
	setAttr ".tk[137]" -type "float3" 0 0.0024613841 0 ;
	setAttr ".tk[138]" -type "float3" 0 0.0047085504 0 ;
	setAttr ".tk[139]" -type "float3" 0 0.0024613841 0 ;
	setAttr ".tk[140]" -type "float3" 0 0.00086260203 0 ;
	setAttr ".tk[141]" -type "float3" 0 0.012430151 0 ;
	setAttr ".tk[142]" -type "float3" 0 0.021700561 0 ;
	setAttr ".tk[143]" -type "float3" 0 0.012430151 0 ;
	setAttr ".tk[144]" -type "float3" 0 0.00086260203 0 ;
	setAttr ".tk[153]" -type "float3" 0 0.0014745247 0 ;
	setAttr ".tk[154]" -type "float3" 0 0.0072483923 0 ;
	setAttr ".tk[155]" -type "float3" 0 0.010508377 0 ;
	setAttr ".tk[156]" -type "float3" 0 0.0072483923 0 ;
	setAttr ".tk[157]" -type "float3" 0 0.0014745247 0 ;
	setAttr ".tk[161]" -type "float3" 0 0.0016504686 0 ;
	setAttr ".tk[162]" -type "float3" 0 0.0070485095 0 ;
	setAttr ".tk[163]" -type "float3" 0 0.010262767 0 ;
	setAttr ".tk[164]" -type "float3" 0 0.0070485095 0 ;
	setAttr ".tk[165]" -type "float3" 0 0.0016504686 0 ;
	setAttr ".tk[175]" -type "float3" 0 0.0014745247 0 ;
	setAttr ".tk[176]" -type "float3" 0 0.0028207214 0 ;
	setAttr ".tk[177]" -type "float3" 0 0.0014745247 0 ;
	setAttr ".tk[182]" -type "float3" 0 0.0017588743 0 ;
	setAttr ".tk[183]" -type "float3" 0 0.0086450102 0 ;
	setAttr ".tk[184]" -type "float3" 0 0.012521519 0 ;
	setAttr ".tk[185]" -type "float3" 0 0.0086450102 0 ;
	setAttr ".tk[186]" -type "float3" 0 0.0017588743 0 ;
	setAttr ".tk[203]" -type "float3" 0 0.0017145418 0 ;
	setAttr ".tk[204]" -type "float3" 0 0.0072451471 0 ;
	setAttr ".tk[205]" -type "float3" 0 0.010506475 0 ;
	setAttr ".tk[206]" -type "float3" 0 0.0072451471 0 ;
	setAttr ".tk[207]" -type "float3" 0 0.0017145418 0 ;
	setAttr ".tk[224]" -type "float3" 0 0.00089627237 0 ;
	setAttr ".tk[225]" -type "float3" 0 0.0044058491 0 ;
	setAttr ".tk[226]" -type "float3" 0 0.0063873939 0 ;
	setAttr ".tk[227]" -type "float3" 0 0.0044058491 0 ;
	setAttr ".tk[228]" -type "float3" 0 0.00089627237 0 ;
	setAttr ".tk[246]" -type "float3" 0 0.00089627237 0 ;
	setAttr ".tk[247]" -type "float3" 0 0.0017145418 0 ;
	setAttr ".tk[248]" -type "float3" 0 0.00089627237 0 ;
	setAttr ".tk[275]" -type "float3" 0 0.0021745088 0 ;
	setAttr ".tk[276]" -type "float3" 0 0.0060165143 0 ;
	setAttr ".tk[277]" -type "float3" 0 0.0021745088 0 ;
	setAttr ".tk[296]" -type "float3" 0 0.0060165143 0 ;
	setAttr ".tk[297]" -type "float3" 0 0.012595315 0 ;
	setAttr ".tk[298]" -type "float3" 0 0.0060165143 0 ;
	setAttr ".tk[309]" -type "float3" 0 0.0014848675 0 ;
	setAttr ".tk[310]" -type "float3" 0 0.0028697099 0 ;
	setAttr ".tk[311]" -type "float3" 0 0.0014848675 0 ;
	setAttr ".tk[317]" -type "float3" 0 0.0021745088 0 ;
	setAttr ".tk[318]" -type "float3" 0 0.0060165143 0 ;
	setAttr ".tk[319]" -type "float3" 0 0.0021745088 0 ;
	setAttr ".tk[329]" -type "float3" 0 0.0014848675 0 ;
	setAttr ".tk[330]" -type "float3" 0 0.0095763076 0 ;
	setAttr ".tk[331]" -type "float3" 0 0.016725454 0 ;
	setAttr ".tk[332]" -type "float3" 0 0.0095763076 0 ;
	setAttr ".tk[333]" -type "float3" 0 0.0014848675 0 ;
	setAttr ".tk[341]" -type "float3" 0 0.0020746847 0 ;
	setAttr ".tk[342]" -type "float3" 0 0.0057403324 0 ;
	setAttr ".tk[343]" -type "float3" 0 0.0020746847 0 ;
	setAttr ".tk[350]" -type "float3" 0 0.0028697099 0 ;
	setAttr ".tk[351]" -type "float3" 0 0.016725454 0 ;
	setAttr ".tk[352]" -type "float3" 0 0.027359419 0 ;
	setAttr ".tk[353]" -type "float3" 0 0.016725454 0 ;
	setAttr ".tk[354]" -type "float3" 0 0.0028697099 0 ;
	setAttr ".tk[359]" -type "float3" 0 0.001126615 0 ;
	setAttr ".tk[360]" -type "float3" 0 0.0031171653 0 ;
	setAttr ".tk[361]" -type "float3" 0 0.001126615 0 ;
	setAttr ".tk[362]" -type "float3" 0 0.0057403324 0 ;
	setAttr ".tk[363]" -type "float3" 0 0.012017158 0 ;
	setAttr ".tk[364]" -type "float3" 0 0.0057403324 0 ;
	setAttr ".tk[371]" -type "float3" 0 0.0014848675 0 ;
	setAttr ".tk[372]" -type "float3" 0 0.0095763076 0 ;
	setAttr ".tk[373]" -type "float3" 0 0.016725454 0 ;
	setAttr ".tk[374]" -type "float3" 0 0.0095763076 0 ;
	setAttr ".tk[375]" -type "float3" 0 0.0014848675 0 ;
	setAttr ".tk[380]" -type "float3" 0 0.0031171653 0 ;
	setAttr ".tk[381]" -type "float3" 0 0.0065256711 0 ;
	setAttr ".tk[382]" -type "float3" 0 0.0031171653 0 ;
	setAttr ".tk[383]" -type "float3" 0 0.0020746847 0 ;
	setAttr ".tk[384]" -type "float3" 0 0.0057403324 0 ;
	setAttr ".tk[385]" -type "float3" 0 0.0020746847 0 ;
	setAttr ".tk[393]" -type "float3" 0 0.0014848675 0 ;
	setAttr ".tk[394]" -type "float3" 0 0.0028697099 0 ;
	setAttr ".tk[395]" -type "float3" 0 0.0014848675 0 ;
	setAttr ".tk[401]" -type "float3" 0 0.001126615 0 ;
	setAttr ".tk[402]" -type "float3" 0 0.0031171653 0 ;
	setAttr ".tk[403]" -type "float3" 0 0.001126615 0 ;
createNode polySoftEdge -n "polySoftEdge4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 24.012908224870134 0 0 0 0 24.012908224870134 0 0 0 0 24.012908224870134 0
		 0 -0.016389332107191734 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak10";
	setAttr ".uopa" yes;
	setAttr -s 13 ".tk";
	setAttr ".tk[8]" -type "float3" 0 0.0068805288 0 ;
	setAttr ".tk[9]" -type "float3" 0 0.28728887 0 ;
	setAttr ".tk[10]" -type "float3" 0 0.052218433 0 ;
	setAttr ".tk[21]" -type "float3" 0 0.17702559 0 ;
	setAttr ".tk[22]" -type "float3" 0 0.35931563 0 ;
	setAttr ".tk[40]" -type "float3" 0 0.001182955 0 ;
	setAttr ".tk[41]" -type "float3" 0 0.0073138252 0 ;
	setAttr ".tk[42]" -type "float3" 0 0.30538067 0 ;
	setAttr ".tk[43]" -type "float3" 0 0.055506848 0 ;
	setAttr ".tk[54]" -type "float3" 0 0.20876463 0 ;
	setAttr ".tk[55]" -type "float3" 0 0.42373756 0 ;
createNode deleteComponent -n "deleteComponent10";
	setAttr ".dc" -type "componentList" 1 "f[5]";
createNode deleteComponent -n "deleteComponent11";
	setAttr ".dc" -type "componentList" 1 "f[3]";
createNode deleteComponent -n "deleteComponent12";
	setAttr ".dc" -type "componentList" 1 "f[3]";
createNode deleteComponent -n "deleteComponent13";
	setAttr ".dc" -type "componentList" 1 "f[8]";
createNode deleteComponent -n "deleteComponent14";
	setAttr ".dc" -type "componentList" 1 "f[7]";
createNode deleteComponent -n "deleteComponent15";
	setAttr ".dc" -type "componentList" 1 "f[6]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :initialShadingGroup;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "deleteComponent15.og" "pPlaneShape1.i";
connectAttr "polySoftEdge2.out" "pPlaneShape2.i";
connectAttr "polySoftEdge4.out" "pPlaneShape3.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyPlane1.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyTweak2.ip";
connectAttr "polyTweak2.out" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polyTweak3.ip";
connectAttr "polyTweak3.out" "deleteComponent3.ig";
connectAttr "|group1|pPlane2|polySurfaceShape1.o" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "deleteComponent5.ig";
connectAttr "deleteComponent5.og" "deleteComponent6.ig";
connectAttr "deleteComponent6.og" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "deleteComponent8.ig";
connectAttr "deleteComponent8.og" "polyTweak5.ip";
connectAttr "polyTweak5.out" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "polySplit3.ip";
connectAttr "polyTweak6.out" "polySplit4.ip";
connectAttr "polySplit3.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polySoftEdge1.ip";
connectAttr "pPlaneShape1.wm" "polySoftEdge1.mp";
connectAttr "deleteComponent3.og" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polySoftEdge2.ip";
connectAttr "pPlaneShape2.wm" "polySoftEdge2.mp";
connectAttr "polySplit4.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polySoftEdge3.ip";
connectAttr "pPlaneShape3.wm" "polySoftEdge3.mp";
connectAttr "polyPlane2.out" "polyTweak9.ip";
connectAttr "polySoftEdge3.out" "polySoftEdge4.ip";
connectAttr "pPlaneShape3.wm" "polySoftEdge4.mp";
connectAttr "polySoftEdge1.out" "polyTweak10.ip";
connectAttr "polyTweak10.out" "deleteComponent10.ig";
connectAttr "deleteComponent10.og" "deleteComponent11.ig";
connectAttr "deleteComponent11.og" "deleteComponent12.ig";
connectAttr "deleteComponent12.og" "deleteComponent13.ig";
connectAttr "deleteComponent13.og" "deleteComponent14.ig";
connectAttr "deleteComponent14.og" "deleteComponent15.ig";
connectAttr "pPlaneShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of 3waysMap.ma
