﻿using UnityEngine;
using System.Collections;

public class LevelTriggerScript : MonoBehaviour {

	public bool laneCheck;
	public bool isActive;
	public bool isPlayerTrigger;

	public bool turnable;
	public int turnToLane;
	public bool disable;
	public bool gameOver;

	void Start () {
		isPlayerTrigger = false;
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.CompareTag ("Player")) {
			if (gameOver) {
				GameScript.Instance.lives = 0;
			} else {
				if (turnable) {
					PlayerScript.Instance.isTurnable = true;
//					StartCoroutine (SlowTime ());
				}

				if (turnToLane != 99) {
					PlayerScript.Instance.playerLane = turnToLane;
					PlayerScript.Instance.prevLane = turnToLane;
				}

				isPlayerTrigger = true;
			}
		}
	}

	void OnTriggerStay (Collider other) {
		if (other.gameObject.CompareTag ("Player")) {
			if (gameOver != true) {
				if (turnable) {				
					PlayerScript.Instance.isTurnable = true;
				}

				if (disable) {
					PlayerScript.Instance.isJumpable = false;
					PlayerScript.Instance.isSwitchingLane = false;
				}
			}
		}
	}

	void OnTriggerExit (Collider other) {
		if (other.gameObject.CompareTag ("Player")) {
			PlayerScript.Instance.isTurnable = false;
			if (gameOver != true) {
				if (laneCheck) {
					laneCheck = false;
					PlayerScript.Instance.checkChangingLane = true;
				}

				if (turnable) {
					PlayerScript.Instance.isTurnable = false;
//					StartCoroutine (NormalTime ());
				}

				if (disable) {
					PlayerScript.Instance.isJumpable = true;
					PlayerScript.Instance.isSwitchingLane = true;
				}
			}
		}
	}

	IEnumerator SlowTime(){
		Time.timeScale = 0.314f;
		yield return null;
	}

	IEnumerator NormalTime(){
		Time.timeScale = 1;
		yield return null;
	}
}
