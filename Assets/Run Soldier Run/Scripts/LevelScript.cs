﻿using UnityEngine;
using System.Collections;

public class LevelScript : MonoBehaviour {

	public int levelType;
	public float levelSpawn;
	public float levelSpawnNextFront;
	public float levelSpawnNextSide;

	public GameObject[] Lanes;
	public LevelTriggerScript TriggerEnd;
	public LevelTriggerScript TriggerDisable;
	public LevelTriggerScript[] TriggerTurn;

	public LevelTriggerScript ReverseTrigger;

	void Start () {
		if (levelType != 0) {
			if (TriggerEnd != null) {
				TriggerEnd.turnable = true;
				TriggerEnd.disable = false;
				TriggerEnd.isActive = false;
				TriggerEnd.laneCheck = true;
				TriggerEnd.turnToLane = 99;
			}

			if (TriggerDisable != null) {
				TriggerDisable.turnable = false;
				TriggerDisable.disable = true;
				TriggerDisable.isActive = false;
				TriggerDisable.laneCheck = false;
				TriggerDisable.turnToLane = 99;
			}

			for (int i = 0; i < TriggerTurn.Length; i++) {
					TriggerTurn [i].turnable = false;
					TriggerTurn [i].isActive = false;
					TriggerTurn [i].laneCheck = false;
					TriggerTurn [i].turnToLane = i;
			}
		} else {
			if (TriggerEnd != null) {
				TriggerEnd.turnable = false;
				TriggerEnd.isActive = true;
				TriggerEnd.laneCheck = false;
				TriggerEnd.turnToLane = 99;
			}
		}
	}

	void Update () {
		if (TriggerEnd != null) {
			if (TriggerEnd.isPlayerTrigger) {
				TriggerEnd.isPlayerTrigger = false;
				StartCoroutine (SelfDestruction ());
				if (TriggerEnd.isActive) {
					LevelSpawnerScript.Instance.spawnLevel = true;
					TriggerEnd.isActive = false;
				}
			}
		}
	}

	IEnumerator SelfDestruction(){
		yield return new WaitForSeconds (1f);
		Destroy (this.gameObject);
	}
}
