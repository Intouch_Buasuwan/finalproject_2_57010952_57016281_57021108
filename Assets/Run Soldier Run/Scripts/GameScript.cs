﻿using UnityEngine;
using System.Collections;

public class GameScript : MonoBehaviour {

	public static GameScript Instance;

	public int lives;
	public int score;
	public int[] scores;
	public float totalTime;
	public int gameDirection;
	public float gameGavity;
	public float gameFallSpeedMax;

	private float totalDistance;

	void Awake() {
		if (Instance != null) {
			Destroy (this.gameObject);
		}

		Instance = this;

		gameDirection = 0;
	}

	void Start() {
		lives = 3;
		score = 0;

		for (int i = 0; i < scores.Length; i++) {
			scores [i] = 0;
		}
	}

	void Update() {
		if (lives > 0) {
			totalTime += 1 * Time.deltaTime;
			totalDistance += PlayerScript.Instance.playerSpeed * Time.deltaTime;

			if (totalDistance >= 1) {
				totalDistance = 0;
				score++;
			}

			StartCoroutine (ScoreToArrayConverter ());
		}
	}

	IEnumerator ScoreToArrayConverter() {
		int n = 0;
		int mod = 10;
		int div = 10;

		while (n < scores.Length) {
			if (n == 0) {
				scores [n] = score % mod;
			} else {
				scores [n] = (score % mod) / div;
				div = div * 10;
			}
			mod = mod * 10;
			n++;
		}

		yield return null;
	}
}
