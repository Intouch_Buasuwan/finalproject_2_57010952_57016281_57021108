﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneSwitchingScript : MonoBehaviour {

	public void SwitchScene (string nameScene) {
		SceneManager.LoadScene (nameScene);
	}
}
