﻿using UnityEngine;
using System.Collections;

public class FollowPlayerScript : MonoBehaviour {

	private Vector3 thisSpeed;
	private PlayerScript target;

	void Start () {
		target = PlayerScript.Instance.GetComponent<PlayerScript> ();
	}

	void Update () {
		thisSpeed = target.mSpeed;
		thisSpeed.y = 0;
		this.transform.position += thisSpeed * Time.deltaTime;
	}
}
