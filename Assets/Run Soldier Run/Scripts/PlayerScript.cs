﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	public static PlayerScript Instance;

	public int playerLane;
	public int prevLane;
	public int playerDirection;
	public float playerSpeed;
	public float playerJump;
	public bool isTurnable;
	public bool isJumpable;
	public bool isSlideable;
	public bool isSwitchingLane;
	public bool checkChangingLane;
	public Vector3 mSpeed;
	public GameObject chPlayer;
	public GameObject[] chPolygon;
	public CharacterController chController;

	private int playerLives;
	private int playerLivesTemp;
	private float gravity;
	private float fallSpeedMax;
	private float timeSlide;
	private float timeSlideTotal;
	private bool isSliding;
	private bool isGamePaused;
	private bool isPlayerTurned;
	private bool checkPlayerGrounded;
	private Animator chAnimator;
	private LevelScript currentLevelScript;
	private Renderer[] chRenderer;
	private Material[] chMaterial;

	void Awake() {
		if (Instance != null) {
			Destroy (this.gameObject);
		}

		Instance = this;
	}

	void Start () {	
		playerLane = 1;
		prevLane = playerLane;
		isSliding = false;
		isTurnable = false;
		isJumpable = true;
		isSlideable = true;
		isGamePaused = false;
		isPlayerTurned = false;
		checkChangingLane = true;
		gravity = GameScript.Instance.gameGavity;
		playerLives = GameScript.Instance.lives;
		playerLivesTemp = playerLives;
		fallSpeedMax = GameScript.Instance.gameFallSpeedMax;
		playerDirection = GameScript.Instance.gameDirection;
		chAnimator = chPlayer.gameObject.GetComponent<Animator> ();
		chRenderer = new Renderer[chPolygon.Length];
		chMaterial = new Material[chPolygon.Length];


		for (int n = 0; n < chRenderer.Length; n++) {
			chRenderer[n] = chPolygon[n].gameObject.GetComponent<Renderer> ();
		}
		for (int n = 0; n < chMaterial.Length; n++) {
			chMaterial[n] = chRenderer[n].material;
		}
	}

	void Update () {
		isGamePaused = UiScript.Instance.isPaused;

		if (isGamePaused != true) {
			if (checkPlayerGrounded) {
				mSpeed.y = 0;
				if (isJumpable) {
					if (Input.GetKey (KeyCode.UpArrow)) {
						mSpeed.y = playerJump;
						chAnimator.SetTrigger ("Jump");
						timeSlide = 2;
						timeSlideTotal = 2;
						isSliding = false;
					}
				}
			} else {
				mSpeed.y += gravity * Time.deltaTime;
				if (mSpeed.y <= fallSpeedMax) {
					mSpeed.y = fallSpeedMax;
				}
			}

			if (isSlideable) {
				if (Input.GetKeyDown (KeyCode.DownArrow)) {
					timeSlide = 0;
					if (isSliding == false) {
						timeSlideTotal = 0;
						StartCoroutine (PlayerIsSliding ());
						isSliding = true;
					}
					mSpeed.y = fallSpeedMax;
				}
			}

			if (playerLives > 0) {
				if (playerDirection == 0) {
					mSpeed.z = playerSpeed;
					mSpeed.x = 0;
					chAnimator.SetFloat ("Speed", playerSpeed);
				} else if (playerDirection == 90) {
					mSpeed.x = playerSpeed;
					mSpeed.z = 0;
					chAnimator.SetFloat ("Speed", playerSpeed);
				} else if (playerDirection == 180) {
					mSpeed.z = -playerSpeed;
					mSpeed.x = 0;
					chAnimator.SetFloat ("Speed", playerSpeed);
				} else if (playerDirection == -90) {
					mSpeed.x = -playerSpeed;
					mSpeed.z = 0;
					chAnimator.SetFloat ("Speed", playerSpeed);
				}

				if (isTurnable) {
					Debug.Log ("isTurnable");
					if (isPlayerTurned == false) {
						if (Input.GetKeyDown (KeyCode.LeftArrow)) {
							Debug.Log ("isTurnable - Turn Left");
							bool isTurningLeft = true;
							playerDirection -= 90;
							if (playerDirection < -90) {
								playerDirection = 180;
							}
							StartCoroutine (PlayerTurning (isTurningLeft));
							isPlayerTurned = true;
						} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
							Debug.Log ("isTurnable - Turn Right");
							bool isTurningLeft = false;
							playerDirection += 90;
							if (playerDirection > 180) {
								playerDirection = -90;
							}
							StartCoroutine (PlayerTurning (isTurningLeft));
							isPlayerTurned = true;
						}
					}
				} else if (isSwitchingLane) {
					if (Input.GetKeyDown (KeyCode.LeftArrow)) {
						Debug.Log ("isSwitchingLane - Left");
						playerLane--;
						isSwitchingLane = false;
						checkChangingLane = true;
					} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
						Debug.Log ("isSwitchingLane - Right");
						playerLane++;
						isSwitchingLane = false;
						checkChangingLane = true;
					}
				}

				if (checkChangingLane) {
					StartCoroutine (PlayerSwitchingLane ());
					checkChangingLane = false;
				}

				playerLives = GameScript.Instance.lives;
				if (playerLives < playerLivesTemp) {
					StartCoroutine (PlayerIsHit ());
					playerLivesTemp = playerLives;
				} else if (playerLives > playerLivesTemp) {
					playerLivesTemp = playerLives;
				}
			} else {
				mSpeed.x = 0;
				mSpeed.z = 0;
			}

			chController.Move (mSpeed * Time.deltaTime);
			checkPlayerGrounded = chController.isGrounded;
		}
	}

	void OnControllerColliderHit (ControllerColliderHit hit){
		if (hit.gameObject.tag == "MapSection") {
			currentLevelScript = hit.gameObject.transform.parent.GetComponent<LevelScript> ();
		}
	}

	IEnumerator PlayerIsSliding () {
		chAnimator.Play ("RDSlideStart");
		chAnimator.SetBool ("Slide", true);

		chController.height = 1.75f;
		Vector3 cenTemp = chController.center;
		cenTemp.y = -0.75f;
		chController.center = cenTemp;

		while (timeSlide < 0.5f && timeSlideTotal < 2) {
			timeSlide += 0.1f;
			timeSlideTotal += 0.1f;
			yield return new WaitForSeconds (0.1f);
		}

		chAnimator.SetBool ("Slide", false);

		chController.height = 3;
		cenTemp.y = 0;
		chController.center = cenTemp;

		isSliding = false;
		yield return null;
	}

	IEnumerator PlayerIsHit () {
		for (int i = 0; i < 3; i++) {
			for (int n = 0; n < chMaterial.Length; n++) {
				Color color = chMaterial[n].color;
				color.a = 0;
				chMaterial[n].color = color;
			}
			yield return new WaitForSeconds (0.1f);
			for (int n = 0; n < chMaterial.Length; n++) {
				Color color = chMaterial[n].color;
				color.a = 1;
				chMaterial[n].color = color;
			}
			yield return new WaitForSeconds (0.1f);
		}

		yield return null;
	}

	IEnumerator PlayerTurning (bool isTurningLeft) {
		if (isTurningLeft) {
			for (int rotated = 0; rotated > -90; rotated -= 15) {
				this.transform.Rotate (0, -15, 0);
				yield return null;
			}
		} else {
			for (int rotated = 0; rotated < 90; rotated += 15) {
				this.transform.Rotate (0, 15, 0);
				yield return null;
			}
		}

		yield return new WaitForSeconds (2f);
		isPlayerTurned = false;
	}

	IEnumerator PlayerSwitchingLane () {
		if (currentLevelScript != null) {
			if (playerLane >= 0 && playerLane <= 2) {
				float speed = 0.3f;
				Vector3 pos = this.transform.position;
				Vector3 lane = currentLevelScript.Lanes [playerLane].transform.position;

				if (playerDirection == 0 || playerDirection == 180) {
					if (playerLane == prevLane) {
						pos.x = lane.x;
						this.transform.position = pos;
					} else if ((playerLane < prevLane && playerDirection == 0) ||
					           (playerLane > prevLane && playerDirection == 180)) {
						for (float n = pos.x; n > lane.x + speed; n -= speed) {
							pos.x -= speed;
							this.transform.position = pos;
							yield return null;
							pos = this.transform.position;
						}
						pos.x = lane.x;
						this.transform.position = pos;
					} else if ((playerLane > prevLane && playerDirection == 0) ||
					           (playerLane < prevLane && playerDirection == 180)) {
						for (float n = pos.x; n < lane.x - speed; n += speed) {
							pos.x += speed;
							this.transform.position = pos;
							yield return null;
							pos = this.transform.position;
						}
						pos.x = lane.x;
						this.transform.position = pos;
					}
				} else if (playerDirection == 90 || playerDirection == -90) {
					if (playerLane == prevLane) {
						pos.z = lane.z;
						this.transform.position = pos;
					} else if ((playerLane < prevLane && playerDirection == 90) ||
					           (playerLane > prevLane && playerDirection == -90)) {
						for (float n = pos.z; n < lane.z - speed; n += speed) {
							pos.z += speed;
							this.transform.position = pos;
							yield return null;
							pos = this.transform.position;
						}
						pos.z = lane.z;
						this.transform.position = pos;
					} else if ((playerLane > prevLane && playerDirection == 90) ||
					           (playerLane < prevLane && playerDirection == -90)) {
						for (float n = pos.z; n > lane.z + speed; n -= speed) {
							pos.z -= speed;
							this.transform.position = pos;
							yield return null;
							pos = this.transform.position;
						}
						pos.z = lane.z;
						this.transform.position = pos;
					}
				}
			} else if (playerLane < 0) {
				playerLane = 0;
			} else if (playerLane > 2) {
				playerLane = 2;
			}
		}

		prevLane = playerLane;
		isSwitchingLane = true;
		yield return null;
	}
}
