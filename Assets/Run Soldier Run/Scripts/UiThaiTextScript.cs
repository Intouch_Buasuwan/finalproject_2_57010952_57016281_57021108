﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UiThaiTextScript : MonoBehaviour {

	public int numType; // 0 = score, 1 = lives
	public int numValue;
	public Sprite[] numbers;

	private int num;
	private Image SlotNumberImage;

	void Start () {
		SlotNumberImage = GetComponent<Image> ();
	}

	void Update () {
		if (numType == 0) {
			num = GameScript.Instance.scores [numValue];
		} else if (numType == 1) {
			num = GameScript.Instance.lives;
			if (num <= 1) {
				SlotNumberImage.color = Color.red;
			} else if (num == 2) {
				Color col = new Color (1, 0.65f, 0, 1);
				SlotNumberImage.color = col;
			} else {
				SlotNumberImage.color = Color.yellow;
			}
		}

		SlotNumberImage.sprite = numbers [num];
	}
}
