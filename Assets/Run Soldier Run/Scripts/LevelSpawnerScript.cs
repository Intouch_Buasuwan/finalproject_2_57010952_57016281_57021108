﻿using UnityEngine;
using System.Collections;

public class LevelSpawnerScript : MonoBehaviour {

	public static LevelSpawnerScript Instance;

	public bool spawnLevel;
	public bool spawnDisable;
	public int currentWorldRotation;
	public GameObject[] Levels;
	public GameObject[] Grounds;
	public GameObject[] Obstacles;

	private Vector3 pos;

	void Awake () {
		if (Instance != null) {
			Destroy (this.gameObject);
		}

		Instance = this;
	}

	void Start() {
		spawnLevel = false;
		spawnDisable = false;
		pos = this.transform.position;
	}

	void Update () {
		if (spawnDisable != true) {
			if (spawnLevel) {
				while (spawnLevel) {
					spawnLevel = false;
					currentWorldRotation = GameScript.Instance.gameDirection;

					int iL = RandomNumberForLevel ();
					int iO = RandomNumberForObstacles ();

					GameObject tempLevel = Instantiate (Levels [iL], this.transform.position, Quaternion.identity) as GameObject;
					GameObject tempGround = Instantiate (Grounds [iL], this.transform.position, Quaternion.identity) as GameObject;
					LevelScript tempLevelScript = tempLevel.gameObject.GetComponent<LevelScript> ();

					if (currentWorldRotation == 0) {
						pos.z += tempLevelScript.levelSpawn;
					} else if (currentWorldRotation == 90) {
						pos.x += tempLevelScript.levelSpawn;
					} else if (currentWorldRotation == 180) {
						pos.z -= tempLevelScript.levelSpawn;
					} else if (currentWorldRotation == -90) {
						pos.x -= tempLevelScript.levelSpawn;
					}

					tempLevel.transform.SetParent (this.transform.parent);
					tempLevel.transform.position = pos;
					tempLevel.transform.Rotate (0, currentWorldRotation, 0);
					tempGround.transform.SetParent (this.transform.parent);
					tempGround.transform.position = pos;
					tempGround.transform.Rotate (0, currentWorldRotation, 0);

					GameObject tempObst = Instantiate (Obstacles [iO], this.transform.position, Quaternion.identity) as GameObject;
					tempObst.transform.SetParent (this.transform.parent);
					tempObst.transform.position = pos;
					tempObst.transform.Rotate (0, currentWorldRotation, 0);

					this.transform.position = pos;

					if (tempLevelScript.levelType != 0) {						
						GameObject tempLevel2 = Instantiate (Levels [0], this.transform.position, Quaternion.identity) as GameObject;

						if (currentWorldRotation == 0) {
							pos.z += tempLevelScript.levelSpawnNextFront;
							pos.x += tempLevelScript.levelSpawnNextSide;
						} else if (currentWorldRotation == 90) {
							pos.x += tempLevelScript.levelSpawnNextFront;
							pos.z -= tempLevelScript.levelSpawnNextSide;
						} else if (currentWorldRotation == 180) {
							pos.z -= tempLevelScript.levelSpawnNextFront;
							pos.x -= tempLevelScript.levelSpawnNextSide;
						} else if (currentWorldRotation == -90) {
							pos.x -= tempLevelScript.levelSpawnNextFront;
							pos.z += tempLevelScript.levelSpawnNextSide;
						}

						if (tempLevelScript.levelType == 4) {
							currentWorldRotation -= 90;
							if (currentWorldRotation < -90) {
								currentWorldRotation = 180;
							}
						} else if (tempLevelScript.levelType == 6) {
							currentWorldRotation += 90;
							if (currentWorldRotation > 180) {
								currentWorldRotation = -90;
							}
						}

						tempLevel2.transform.SetParent (this.transform.parent);
						tempLevel2.transform.position = pos;
						tempLevel2.transform.Rotate (0, currentWorldRotation, 0);

						int iO2 = RandomNumberForObstacles ();
						GameObject tempObst2 = Instantiate (Obstacles [iO2], this.transform.position, Quaternion.identity) as GameObject;
						tempObst2.transform.SetParent (this.transform.parent);
						tempObst2.transform.position = pos;
						tempObst2.transform.Rotate (0, currentWorldRotation, 0);

						this.transform.position = pos;
					}

					GameScript.Instance.gameDirection = currentWorldRotation;
				}
			}
		}
	}

	int RandomNumberForLevel () {
		int num = Random.Range (0, 100); //Percentage of spawing Levels

		if (num >= 40 && num < 70) {
			num = 1;
		} else if (num >= 70 && num < 100) {
			num = 2;
		} else {
			num = 0;
		}

		return num;
	}

	int RandomNumberForObstacles () {
		int num = Random.Range (0, Obstacles.Length);

		return num;
	}
}
