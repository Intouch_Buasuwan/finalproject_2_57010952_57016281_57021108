﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UiScript : MonoBehaviour {

	public static UiScript Instance;

	public bool isPaused;
	public Image PauseMenu;
	public GameObject Resume01;
	public GameObject Resume02;

	private bool isPausing;
	private Animator PauseMenuAnimator;

	void Awake () {
		if (Instance != null) {
			Destroy (this.gameObject);
		}

		Instance = this;
	}

	void Start () {
		isPaused = false;
		isPausing = false;
		PauseMenuAnimator = PauseMenu.GetComponent<Animator> ();
	}

	public void PauseButtonDown() {
		if (isPausing == false) {
			if (isPaused == true) {
				isPausing = true;
				PauseMenuAnimator.Play ("PauseMenuResume");
				Resume ();

			} else if (isPaused == false) {
				isPausing = true;
				PauseMenuAnimator.Play ("PauseMenuPause");
				Pause ();
			}
		}
	}

	public void Pause() {
		StartCoroutine (PauseSequence ());
	}

	public void Resume() {
		StartCoroutine (ResumeSequence ());
	}

	IEnumerator PauseSequence() {
		Time.timeScale = 0;
		isPaused = true;

		for (float t = 0; t < 1; t += 1 * Time.unscaledDeltaTime) {
			yield return null;
		}

		isPausing = false;

		yield return null;
	}

	IEnumerator ResumeSequence() {
		if (GameScript.Instance.lives > 0) {
			Resume01.gameObject.SetActive (true);

			for (float t = 0; t < 1; t += 1 * Time.unscaledDeltaTime) {
				yield return null;
			}

			Resume01.gameObject.SetActive (false);
			Resume02.gameObject.SetActive (true);

			for (float t = 0; t < 1; t += 1 * Time.unscaledDeltaTime) {
				yield return null;
			}

			Resume02.gameObject.SetActive (false);
			isPaused = false;
			Time.timeScale = 1;
		}

		isPausing = false;

		yield return null;
	}
}
