﻿using UnityEngine;
using System.Collections;

public class ObstacleScript : MonoBehaviour {

	private BoxCollider bCollider;

	void Start() {
		bCollider = this.gameObject.GetComponent<BoxCollider> ();
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.CompareTag ("Player")) {
			Debug.Log ("Hit " + other);
			bCollider.enabled = false;
			GameScript.Instance.lives--;
		}
	}
}
